package com.testapp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Floor {

    private int numFloor;
    private List <Integer> up;
    private List <Integer> down;

    Floor(int numFloor) {
        this.numFloor = numFloor;
        up = new LinkedList<>();
        down = new LinkedList<>();
    }

    /**
     * This funk add person to the floor
     * @param k - floor where this person want to go;
     */
    void addPerson(int k){
       if(k == numFloor){
           return;
       }
       if(k  > numFloor){
           up.add(k);
       } else {
           down.add(k);
       }
    }

    boolean isSomeone(){
        return isSomeoneGoingDown() || isSomeoneGoingUp();
    }

    /**
     * Return true is someone want to go down
     */
    boolean isSomeoneGoingUp(){
        return !up.isEmpty();
    }

    /**
     * Return true is someone want to go down
     */
    boolean isSomeoneGoingDown(){
        return !down.isEmpty();
    }

    /**
     * This funk return some people wich want to go up and remove them from up list
     * @param n - quantity of persons
     * @return list of persons wich want go up
     */
    List<Integer> getPeopleGoingUp(int n){
        return getFirstsFromList(n, up);
    }

    /**
     * This funk return some people wich want to go down and remove them from down list
     * @param n - quantity of persons
     * @return list of persons wich want go down
     */
    List<Integer> getPeopleGoingDown(int n){
        return getFirstsFromList(n, down);
    }


    private List<Integer> getFirstsFromList(int n, List<Integer> list){
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < n && i < list.size(); i++) {
            res.add(list.get(i));
        }
        if (res.size() > 0) {
            list.subList(0, res.size()).clear();
        }
        return res;
    }

    @Override
    public String toString() {
        String t = ", ";
        if(up.size() == 0 || down.size() == 0)
            t = "";
        return (Arrays.toString(up.toArray()) + t + Arrays.toString(down.toArray())).replace("]", "").replace("[", "");
    }
}
