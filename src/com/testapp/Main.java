package com.testapp;

import java.util.Random;
import java.util.Scanner;

public class Main {

    private Random random;
    private Floor[] floors;
    private Elevator elevator;

    public static void main(String[] args) {
        new Main().start();
    }

    private void start(){
        Scanner scanner = new Scanner(System.in);
        elevator = new Elevator(5);
        fillFloors();
        System.out.println("floor quantity = " + floors.length);
        show();
        System.out.println("for continue print any symbol and press enter");
        scanner.next();
        while (true){
            elevator.releasedPeople();
            if(elevator.getWay() == Elevator.Way.UP) {
                elevator.addPeople(floors[elevator.getCurrentFloor()].getPeopleGoingUp(elevator.getEnptySize()));
            } else if (elevator.getWay() == Elevator.Way.DOWN){
                elevator.addPeople(floors[elevator.getCurrentFloor()].getPeopleGoingDown(elevator.getEnptySize()));
            } else {
                if(floors[elevator.getCurrentFloor()].isSomeoneGoingDown()){
                    elevator.addPeople(floors[elevator.getCurrentFloor()].getPeopleGoingDown(elevator.getEnptySize()));
                } else if(floors[elevator.getCurrentFloor()].isSomeoneGoingUp()){
                    elevator.addPeople(floors[elevator.getCurrentFloor()].getPeopleGoingUp(elevator.getEnptySize()));
                }
            }
            show();
            int nearestFloor = getNearestFloor();
            elevator.setCurrentFloor(nearestFloor);
            System.out.println("for continue print any symbol and press enter");
            scanner.next();
        }
    }

    private void fillFloors() {
        floors = new Floor[getRandomInt(5, 20)];
        for (int i = 0; i < floors.length; i++) {
            int q = getRandomInt(0, 10);
            floors[i] = new Floor(i);
            for (int j = 0; j < q; j++) {
                floors[i].addPerson(getNumWantedFloor(i, floors.length));
            }
        }
    }

    private int getNearestFloor() {
        int f = 0;
        if (elevator.getWay() == Elevator.Way.NON) {
            for (int i = elevator.getCurrentFloor(); i < floors.length; i++) {
                if (floors[i].isSomeone()) {
                    f = i;
                    break;
                }
            }
            for (int i = 0; i < elevator.getCurrentFloor(); i++) {
                if (floors[i].isSomeone() && f - elevator.getCurrentFloor() < elevator.getCurrentFloor() - i) {
                    f = i;
                    break;
                }
            }
        } else if (elevator.getWay() == Elevator.Way.UP) {
            int t = 100;
            for (int i = elevator.getCurrentFloor() + 1; i < floors.length; i++) {
                if (floors[i].isSomeoneGoingUp() && elevator.getEnptySize() > 0) {
                    t = i;
                    break;
                }
            }
            f = Math.min(elevator.getNeedFloor(), t);
        } else {
            int t = -1;
            for (int i = elevator.getCurrentFloor()-1; i > 0; i--) {
                if (floors[i].isSomeoneGoingDown() && elevator.getEnptySize() > 0) {
                    t = i;
                    break;
                }
            }
            f = Math.max(elevator.getNeedFloor(), t);

        }
        return f;
    }

    private void show() {
        for (int i = floors.length - 1; i >= 0 ; i--) {
            System.out.print(i + "\t|");
            if(elevator.getCurrentFloor() == i) {
                System.out.print("| " + elevator.toString() + " || ");
            } else {
                System.out.print("\t | ");
            }
            System.out.println(floors[i].toString());
        }
    }

    private int getRandomInt(int min, int max){
        if(random == null){
            random = new Random();
        }
        return random.nextInt(max - min) + min;
    }

    private int getNumWantedFloor(int currentFloor, int quantityFloors){
        if(getRandomInt(0, quantityFloors) >= currentFloor){
            return getRandomInt(currentFloor, quantityFloors);
        } else {
            return getRandomInt(0, currentFloor);
        }
    }
}
