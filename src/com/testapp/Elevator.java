package com.testapp;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Elevator {

    public static enum Way{
        UP, DOWN, NON
    }

    private int size;
    private List<Integer> people;
    private int currentFloor;

    Elevator(int size) {
        this.size = size;
        this.people = new LinkedList<>();
        currentFloor = 0;
    }

    int getNeedFloor(){
        return people.size() > 0 ? people.get(0) : -1;
    }

    Way getWay() {
        for (Integer integer : people) {
            if (integer > currentFloor) {
                return Way.UP;
            } else if (integer < currentFloor) {
                return Way.DOWN;
            }
        }
        return Way.NON;
    }

    void releasedPeople(){
        List<Integer> res = new LinkedList<>();
        for (Integer person : people) {
            if (person != currentFloor) {
                res.add(person);
            }
        }
        people = res;
    }

    int getEnptySize(){
        return size - people.size();
    }

    void addPeople(List<Integer> people) {
        this.people.addAll(people);
        if (getWay() == Way.UP) {
            this.people.sort((o1, o2) -> o1 - o2);
        } else {
            this.people.sort(((o1, o2) -> o2 - o1));
        }
    }

    int getCurrentFloor() {
        return currentFloor;
    }

    void setCurrentFloor(int currentFloor) {
        this.currentFloor = currentFloor;
    }

    @Override
    public String toString(){
        return Arrays.toString(people.toArray()).replace("[", "").replace("]", "");
    }
}
